"use strict";

var should = require('should');
var sinon = require('sinon');
var mongoose = require('mongoose');

require('sinon-mongoose');

var UserModel = require('../../../app/models/user.model');

describe('UserController testing', function () {

	describe('User Post test', function () {
		
		it('Should call save only once', function () {
			var saveStub = sinon.stub();
			function Book(){
				this.save = saveStub
			}
			var req = {
				body: {
					todo: "Test user from mock"
				}
			}
			var res = {}, next = {};
			var UserController = require('../../../app/controllers/user.controller')(Book);
			UserController.PostUser(req, res, next);
			sinon.assert.calledOnce(saveStub);
		});

		it('Should save todo', function (done) {
			var userMock = sinon.mock(new UserModel({ todo: 'Save new todo from mock'}));
			var user = userMock.object;

			userMock
			.expects('save')
			.yields(null, 'SAVED');

			user.save(function(err, result) {
				userMock.verify();
				userMock.restore();
				should.equal('SAVED', result, "Test fails due to unexpected result")
				done();
			});
		});

	});

	describe('Get all User test', function () {
		it('Should call find once', function (done) {
			var UserMock = sinon.mock(UserModel);
			UserMock
			.expects('find')
			.yields(null, 'TODOS');

			UserModel.find(function (err, result) {
				UserMock.verify();
				UserMock.restore();
				should.equal('TODOS', result, "Test fails due to unexpected result")
				done();
			});
		});
	});

	describe('Delete user test', function () {
		it('Should delete user of gived id', function (done) {
			var UserMock = sinon.mock(UserModel);

			UserMock
			.expects('remove')
			.withArgs({_id: 12345})
			.yields(null, 'DELETED');

			UserModel.remove({_id: 12345}, function(err, result){
				UserMock.verify();
				UserMock.restore();
				done();
			})


		});
	});

	describe('Update a user', function () {
		it('Should update the user with new value', function (done) {
			var userMock = sinon.mock(new UserModel({ todo: 'Save new user from mock'}));
			var user = userMock.object;

			userMock
			.expects('save')
			.withArgs({_id: 12345})
			.yields(null, 'UPDATED');

			user.save({_id: 12345}, function(err, result){
				userMock.verify();
				userMock.restore();
				done();
			})

		});
	});

});
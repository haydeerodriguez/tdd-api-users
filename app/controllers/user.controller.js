"use strict";

var UserCtrl = function(User){

	var UsrObj = {};
	//Method for post an user in the database
	UsrObj.PostUser = function(req, res, next){
		var newUser = new User(req.body);
		newUser.save(function(err, todo){
			if(err){
				res.json({status: false, error: err.message});
				return;
			}
			res.json({status: true, todo: user});
		});
	}

	UsrObj.GetUser = function(req, res, next){
		User.find(function(err, users){
			if(err) {
				res.json({status: false, error: "Something went wrong"});
				return
			}
			res.json({status: true, todo: users});
		});
	}

	UsrObj.UpdateUser = function(req, res, next){
		var status = req.body.status;
		User.findById(req.params.todo_id, function(err, user){
			user.status = status;
			user.save(function(err, user){
				if(err) {
					res.json({status: false, error: "Status not updated"});
				}
				res.json({status: true, message: "Status updated successfully"});
			});
		});
	}

	UsrObj.DeleteUser = function(req, res, next){
		User.remove({_id : req.params.todo_id }, function(err, users){
			if(err) {
				res.json({status: false, error: "Deleting user is not successfull"});
			}
			res.json({status: true, message: "Todo deleted successfully"});
		});
	}

	return UsrObj;
}

module.exports = UserCtrl;

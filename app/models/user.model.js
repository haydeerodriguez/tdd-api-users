var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// TODO schema
var UserSchema = new Schema({
	name: String,
	status: { type:Boolean, default: false },
	created_by: { type: Date, default: Date.now }
});

// True since it is a parallel middleware
UserSchema.pre('save', function(next, done) {
	if(!this.todo){
		next(new Error("User should not be null"));
	}
  	next();
});

var UserModel = mongoose.model('User', UserSchema);

module.exports = UserModel;